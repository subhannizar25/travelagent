package models

import (
	"github.com/revel/revel"
	"regexp"
)

type User struct {
	UID int
	Nama	string
	Email	string
	Username,Password	string
	HashedPassword	[]byte
	AccessToken string
}

var emailregex = regexp.MustCompile(`^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`)
//var userregex = regexp.MustCompile(`^\\w*$`)
func(user *User) Validation(r *revel.Validation){
	r.Check(user.Nama,
		revel.Required{},
		revel.MinSize{4},
		revel.MaxSize{20},
	)
	r.Check(user.Username,
		revel.Required{},
		revel.MinSize{4},
		revel.MaxSize{20},

		//revel.Match{userregex},
	)
	r.Check(user.Email,
		revel.Required{},
		revel.MinSize{4},
		revel.Match{emailregex},
	)
	ValidatePassword(r, user.Password).Key("User.Password")

}

func ValidatePassword(v *revel.Validation, password string) *revel.ValidationResult {
	return v.Check(password,
		revel.Required{},
		revel.MaxSize{15},
		revel.MinSize{5},
	)
}
