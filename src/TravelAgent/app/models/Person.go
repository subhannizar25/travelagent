package models

import (
	"time"
	"github.com/revel/revel"
)

type Person struct {
	IdPerson	int
	Nama		string		`json:"Nama"`
	Alamat		string		`json:"Alamat"`
	TempatLahir	string		`json:"TempatLahir"`
	TanggalLahir	time.Time	`json:"TanggalLahir"`
	Pekerjaan	string		`json:"Pekerjaan"`

}
const (
	SQL_DATE_FORMAT = "2006-01-02"
)

func (person *Person) Validation(v *revel.Validation){
	v.Check(person.Nama,
		revel.Required{},
		revel.MinSize{4},
		revel.MaxSize{5},
	)
	v.Check(person.Alamat,
		revel.Required{},
		revel.MinSize{8},
		revel.MaxSize{40},
	)
	v.Check(person.TempatLahir,
		revel.Required{},
		revel.MinSize{4},
		revel.MaxSize{20},
	)
	person.TanggalLahir.Format(SQL_DATE_FORMAT)


}



