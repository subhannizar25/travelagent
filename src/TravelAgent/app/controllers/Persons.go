package controllers

import (
	"github.com/revel/revel"
	"TravelAgent/app/models"
	"TravelAgent/app/routes"
	"strings"
	"time"
)

type Persons struct {
	App
	Database
}
func (c Persons) checkUser() revel.Result {
	if user := c.connected(); user == nil {
		c.Flash.Error("Please log in first")
		return c.Redirect(routes.App.Login())
	}
	return nil
}
func (c Persons) List(search string) revel.Result{
	if user := c.connected(); user == nil {
		c.Flash.Error("Please log in first")
		return c.Redirect(routes.App.Login())
	}
	sr:=""
	if search==""{
		sr = strings.TrimSpace(search)
	}else{
		sr = strings.ToLower(search)
	}

	persons, err := c.Database.initDb().Select(models.Person{},`SELECT * FROM public."Person" WHERE "Nama" LIKE '%`+sr+`%'`)
	if err!=nil{
		panic(err)
	}
	return c.Render(persons)
}
func(c Persons) Show(id int)revel.Result{
	if user := c.connected(); user == nil {
		c.Flash.Error("Please log in first")
		return c.Redirect(routes.App.Login())
	}
	persons:=c.getPerson(id)
	return c.Render(persons)
}
func (c Persons) Tambah()revel.Result{
	if user := c.connected(); user == nil {
		c.Flash.Error("Please log in first")
		return c.Redirect(routes.App.Login())
	}
	return c.Render()
}
func(c Persons) AddData(person *models.Person)revel.Result{
	c.Validation.Required(person.Nama)
	c.Validation.Required(person.Alamat)
	c.Validation.Required(person.TempatLahir)
	c.Validation.Required(person.TanggalLahir)
	c.Validation.Required(person.Pekerjaan)
	person.Validation(c.Validation)
	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(routes.Persons.Tambah())
	}
	if user := c.connected(); user == nil {
		c.Flash.Error("Please log in first")
		return c.Redirect(routes.App.Login())
	}
	err:= c.Database.initDb().Insert(person)

	CheckError(err)
	return  c.Redirect(routes.Persons.List(""))
}

func(c Persons) Delete(id int)revel.Result{
	if user := c.connected(); user == nil {
		c.Flash.Error("Please log in first")
		return c.Redirect(routes.App.Login())
	}
	_,err:=c.initDb().Delete(&models.Person{IdPerson:id})
	if err!=nil{
		panic(err)
	}
	return c.Redirect(routes.Persons.List(""))
}


func(c Persons) Ubah(nama, alamat, tempatlahir, pekerjaan string, tanggallahir time.Time, id int)revel.Result{
	if user := c.connected(); user == nil {
		c.Flash.Error("Please log in first")
		return c.Redirect(routes.App.Login())
	}
	person:=c.getPerson(id)
	person.Nama=nama
	person.Alamat=alamat
	person.TempatLahir=tempatlahir
	person.TanggalLahir=tanggallahir
	person.Pekerjaan=pekerjaan
	_,err:= c.Database.initDb().Update(person)
	CheckError(err)
	return  c.Redirect(routes.Persons.List(""))
}

func (c Persons) GetData(id int) revel.Result{
	if user := c.connected(); user == nil {
		c.Flash.Error("Please log in first")
		return c.Redirect(routes.App.Login())
	}
	person:=c.getPerson(id)
	return c.Render(person)
}

func (c Persons) getPerson(id int) *models.Person{
	person,err:=c.Database.initDb().Get(models.Person{},id)

	if err!=nil{
		panic(err)
	}
	p := person.(*models.Person)
	return p
}

func (c Persons) Logout() revel.Result {
	if user := c.connected(); user == nil {
		c.Flash.Error("Please log in first")
		return c.Redirect(routes.App.Login())
	}
	for k := range c.Session {
		delete(c.Session, k)
	}
	return c.Redirect(routes.App.Index())
}