package controllers

import (
	"github.com/revel/revel"
	"TravelAgent/app/models"
	"TravelAgent/app/routes"
	"golang.org/x/crypto/bcrypt"
	//"golang.org/x/oauth2"
	//"golang.org/x/oauth2/facebook"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/facebook"
)

type App struct {
	*revel.Controller
	Database
}

type AccessToken struct {
	Token string
	Expiry int64
}
var FACEBOOK = &oauth2.Config{
	ClientID:     "1232747110132777",
	ClientSecret: "4daa48f88b21adf49913e3d9abed3faa",
	Scopes:       []string{},
	Endpoint:     facebook.Endpoint,
	RedirectURL:  "http://keylacollection.esy.es/",
}

func (c App) connected() *models.User {
	if c.RenderArgs["user"] != nil {
		return c.RenderArgs["user"].(*models.User)
	}
	if username, ok := c.Session["user"]; ok {
		return c.GetUser(username)
	}
	return nil
}

func (c App) SetUser() revel.Result {
	user := c.connected()
	if user != nil {
		c.RenderArgs["user"] = user
	}
	return nil
}
func (c App) Index() revel.Result {
	if c.connected() != nil {
		return c.Redirect(routes.Persons.List(""))
	}
	return c.Render()
}
func (c App) Register() revel.Result {
	if c.connected() != nil {
		return c.Redirect(routes.Persons.List(""))
	}
	return c.Render()
}
func (c App) Login() revel.Result {
	if c.connected() != nil {
		return c.Redirect(routes.Persons.List(""))
	}
	return c.Render()
}

func (c App) GetUser(username string) *models.User{
	users, err:=c.initDb().Select(models.User{}, `SELECT * FROM public."User" WHERE "Username"='`+username+`'`)
	CheckError(err)
	if len(users)==0{
		return nil
	}
	return users[0].(*models.User)
}

func (c App) AddUser(user models.User, verifyPassword string)revel.Result{
	c.Validation.Required(verifyPassword)
	c.Validation.Required(verifyPassword == user.Password).
		Message("Password Not Match")
	user.Validation(c.Validation)
	if c.Validation.HasErrors(){
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(routes.App.Register())
	}
	user.HashedPassword, _=bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	err:=c.Database.initDb().Insert(&user)
	CheckError(err)
	return c.Redirect(routes.App.Index())
}

func (c App) Auth(username, password string, remember bool)revel.Result{
	user:=c.GetUser(username)
	if user!=nil{
		err:=bcrypt.CompareHashAndPassword(user.HashedPassword, []byte(password))
		if err==nil{
			c.Session["user"]=user.Username
			if remember{
				c.Session.SetDefaultExpiration()
			}else{
				c.Session.SetNoExpiration()
			}
			c.Flash.Success("Wellcome here, "+ user.Username)
			c.RenderArgs["user"]=username
			return c.Redirect(routes.Persons.List(""))
		}
	}
	c.Flash.Out["username"]=username
	c.Flash.Error("Login Failed")
	return c.Redirect(routes.App.Login())
}



func (c App) RegisterWithFacebook() revel.Result{
	u:=c.connected()
	if u!=nil && u.AccessToken!=""{
		c.Flash.Error("Anda sudah memiliki akun, Silahkan Login")
		return c.Redirect(routes.App.Register())
	}
	authUrl := FACEBOOK.AuthCodeURL("state", oauth2.AccessTypeOnline)
	return  c.Render(authUrl)
}


