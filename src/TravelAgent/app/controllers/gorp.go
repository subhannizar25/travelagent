package controllers

import (
	"github.com/go-gorp/gorp"
	"fmt"
	"database/sql"
	"TravelAgent/app/models"
	_ "github.com/lib/pq"

)

type Database struct {

}


const (
	DB_USER	= "postgres"
	DB_PASS	= "k3yf4t0n"
	DB_NAME	= "db_prog"
)

func(datab *Database) initDb() *gorp.DbMap{
	dbinfo	:= fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable", DB_USER, DB_PASS, DB_NAME)
	db,err:=sql.Open("postgres", dbinfo)
	CheckError(err)

	dbmap:=&gorp.DbMap{Db:db, Dialect:gorp.PostgresDialect{}}

	dbmap.AddTableWithName(models.Person{},"Person").SetKeys(true, "IdPerson")
	err1:=dbmap.CreateTablesIfNotExists()
	CheckError(err1)
	dbmap.AddTableWithName(models.User{},"User").SetKeys(true, "UID")
	err2:=dbmap.CreateTablesIfNotExists()
	CheckError(err2)
	return dbmap
}

func CheckError(err error)  {
	if err!=nil{
		panic(err)
	}
}